import { Component, OnInit } from '@angular/core';

import { FormBuilder,FormControl,FormGroup,Validators} from "@angular/forms"
import { Router, ActivatedRoute } from '@angular/router';
import { Auth } from 'aws-amplify';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/Services/auth/auth.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  registerForm : FormGroup

  submitted = false

  constructor(private formbuilder:FormBuilder,
              private router: Router,
              private toastr:ToastrService,
              private auth:AuthService) { 
      
      if(auth.checkIfAuthenticated()){
        this.router.navigateByUrl('/home');
      }
    }

  ngOnInit(): void {
    this.registerForm = this.formbuilder.group({
      email : ['',[Validators.required,Validators.email]],
      password :['',[Validators.required,Validators.minLength(8)]]
    })
  }


  get h(){
    return this.registerForm.controls;
  }

  onSubmit(){
    this.submitted = true

    if(this.registerForm.invalid){
      console.log("INVALID");    
      return;  
    }

    const {email,password} = this.registerForm.value;
    const username = email
    const user = {username,email,password}

    this.auth.signup(user).then((data)=>{
      const user = data.user.getUsername()        
        this.toastr.success("Signed up successfully")        
        this.router.navigateByUrl("/verification")
        }).catch((err)=>{
          this.toastr.error("Sign up failed")
        });
    }  
}
