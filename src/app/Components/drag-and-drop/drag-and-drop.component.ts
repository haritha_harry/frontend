import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {TASKS,INCOMPLETE,INPROGRESS,COMPLETED} from '../../constants/tasks'
import { DragAndDropService } from 'src/app/services/dragAndDrop/drag-and-drop.service';
@Component({
  selector: 'app-drag-and-drop',
  templateUrl: './drag-and-drop.component.html',
  styleUrls: ['./drag-and-drop.component.scss']
})
export class DragAndDropComponent implements OnInit {

  constructor(private dragAndDropService:DragAndDropService) { }

  ngOnInit(): void {
  }

  data=[
    {
      id:1,
      name:"add authentication",
      description:["enter user details and authenticate"],
      status:'incomplete',
      time:'11:44'
    },
    {
      id:2,
      name:"add navbar",
      description:["add navigation bar at the top "],
      status:'incomplete',
      time:'01:44'
    },
    {
      id:3,
      name:"bug fix",
      description:["user details are not storing in the database"],
      status:'incomplete',
      time:'12:34'
    },
    {
      id:4,
      name:"enable file upload",
      description:["add a feature for the file upload for storing user image and the image size should be less than 6Mb"],
      status:'inProgress',
      time:'03:12',
      email:'user email',
    },
    {
      id:5,
      name:"Email feature",
      description:["add a email feature to rest the password "],
      status:'completed',
      time:'06:01'
    },
    {
      id:6,
      name:"create profile dashboard",
      description:["a component to see all the details of the user"],
      status:'completed',
      time:'08:00'
    },
  ]

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer != event.container)
    {
      console.log(event.container.id,event.previousContainer.id);
      if(event.container.id=="cdk-drop-list-1" && event.previousContainer.id=="cdk-drop-list-0"){
        this.data[event.previousIndex].status=INPROGRESS;
        this.dragAndDropService.updateStatus(this.data[event.previousIndex]);
      }

      if(event.container.id=="cdk-drop-list-2" && event.previousContainer.id=="cdk-drop-list-1"){
        this.data[event.previousIndex].status=COMPLETED;
        this.dragAndDropService.updateStatus(this.data[event.previousIndex]);
      }
      if(event.container.id=="cdk-drop-list-2" && event.previousContainer.id=="cdk-drop-list-0"){
        this.data[event.previousIndex].status=COMPLETED;
        this.dragAndDropService.updateStatus(this.data[event.previousIndex]);
      }
      if(event.container.id=="cdk-drop-list-3" && event.previousContainer.id=="cdk-drop-list-0"){
        this.data.splice(event.previousIndex,1);
        this.dragAndDropService.deleteTask();
      }
      console.log(this.data)

    }
  }

checkStatus(status:string){
  if(status==INCOMPLETE)
  return true;
  return false
}
checkProgress(status:string){
  if(status==INPROGRESS)
  return true;
  return false
}

isComplete(status:string){
  if(status==COMPLETED)
  return true;
  return false;
}


}
