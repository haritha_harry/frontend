import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor() { }

  projectDetails = [
    {"id" : 1, "name" : "Hiring Management", "progress" : "30"},
    {"id" : 2, "name" : "Hospital Administration", "progress" : "50"},
    {"id" : 3, "name" : "Video Calling App", "progress" : "70"},
    {"id" : 4, "name" : "Office Management", "progress" : "90"}
  ];
  projectCount = [
    {"id" : 1, "name" : "Projects Count", "count" : "35", "logo" : "view_in_ar"},
    {"id" : 2, "name" : "Front-End Developers", "count" : "40", "logo" : "developer_board"},
    {"id" : 3, "name" : "Back-End Developers", "count" : "45", "logo" : "developer_mode"},
    {"id" : 4, "name" : "Total Employees", "count" : "120", "logo" : "account_circle"}
  ];
  getProjects() {
    return this.projectDetails;
  }
  getProjectsCount() {
    return this.projectCount;
  }
}
