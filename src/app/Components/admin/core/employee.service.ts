import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }

  employeeDetails = [
    {"id" : "1", "name" : "Govind K", "designation" : "Front-end developer", "email" : "govind.k@presidio.com"},
    {"id" : "2", "name" : "Gowtham S", "designation" : "Front-end developer", "email" : "gowtham.sureshbabu@presidio.com"},
    {"id" : "3", "name" : "Haritha  K J", "designation" : "Front-end developer", "email" : "haritha.kj@presidio.com"},
    {"id" : "4", "name" : "Vishal P K", "designation" : "Front-end developer", "email" : "vishal.pk@presidio.com"}
  ];

  getEmployees() {
    return this.employeeDetails;
  }
}
