import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../core/projects.service';

@Component({
  selector: 'app-peoples',
  templateUrl: './peoples.component.html',
  styleUrls: ['./peoples.component.scss']
})
export class PeoplesComponent implements OnInit {

  countValues;
  constructor( private projectsService : ProjectsService) { }

  ngOnInit(): void {
    this.countValues = this.projectsService.getProjectsCount();
  }

}
