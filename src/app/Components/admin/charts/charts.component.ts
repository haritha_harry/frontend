import { Component, OnInit } from '@angular/core';
import { Chart } from 'node_modules/chart.js';
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {
  LineChart;
  barChart
  constructor() { }

  ngOnInit(): void {
    this.LineChart = new Chart('lineChart', {
      type : 'line',
      data : {
        labels : ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept"],
        datasets : [{
          label : 'Projects Completed',
          data : [1,2,2,4,6,7,7,7,8],
          fill : true,
          lineTension : 0.1,
          borderColor : "#63C2FF",
          borderWidth : 4,
          order : 9
        }]
      },
      options :  {
        responsive :false,
      //   title : {
      //     text : "Projects Completed",
      //     display : true
      // },
      scales : {
        yAxes : [{
          ticks : {
            beginAtZero : true
          }
        }]
      }
    }
    });

    this.barChart = new Chart('barChart', {
      type: 'bar',
      data: {
          labels: ['2014', '2015', '2017', '2018', '2019', '2020'],
          datasets: [{
              label : 'Revenue Earned',
              data: [7, 10, 4, 8, 10, 13],
              backgroundColor: [
                  '#63C2FF',
                  '#FF7963',
                  '#C8FB1B ',
                  '#1BFBAC',
                  '#D86CF8 ',
                  '#F86C75 '
              ],
              borderWidth: 2
          }]
      },
      options: {
        responsive : false,
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
  }
 
}
