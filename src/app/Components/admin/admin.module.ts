import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatProgressBarModule} from '@angular/material/progress-bar';

import { AdminComponent } from './admin.component';
import { PeoplesComponent } from './peoples/peoples.component';
import { PeopleListComponent } from './people-list/people-list.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ChartsComponent } from './charts/charts.component';



@NgModule({
  declarations: [AdminComponent, PeoplesComponent, PeopleListComponent, ProjectListComponent, ChartsComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatTableModule,
    MatProgressBarModule
  ],
  exports : [
   AdminComponent  
  ]
})
export class AdminModule { }
