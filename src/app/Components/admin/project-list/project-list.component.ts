import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../core/projects.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  dataSource = [];
  displayedColumns: string[] = ['id', 'name', 'progress'];

  constructor(private projectService : ProjectsService) { }

  ngOnInit(): void {
    this.dataSource = this.projectService.getProjects();
  }

}
