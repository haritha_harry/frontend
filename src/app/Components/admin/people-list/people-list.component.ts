import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../core/employee.service';
@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {

  dataSource : Object = {};
  displayedColumns: string[] = ['id', 'name', 'designation', 'email'];
  constructor(private employeeService : EmployeeService) { }

  ngOnInit(): void {
    this.dataSource = this.employeeService.getEmployees();
    console.log(this.dataSource);
  }

}
