import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../Services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  username =null

  constructor(private auth:AuthService,
              private router:Router,
              private toastr : ToastrService) {    
   }

  ngOnInit(): void {
    
  } 

  isSignedIn(){
    if(localStorage.getItem('signin')!=null){
      return true
    }
    return false
  }  
  

  async handleSignout(){
    this.auth.signout()
    .then((data)=>{
      this.toastr.success("Logout success")
    })
    .catch((err)=>{
    });  
    this.username=""
    localStorage.clear();
    this.router.navigateByUrl("/signin");  
    }
}
