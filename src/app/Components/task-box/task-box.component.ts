import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-task-box',
  templateUrl: './task-box.component.html',
  styleUrls: ['./task-box.component.scss']
})
export class TaskBoxComponent implements OnInit {
  @Input('item') item
  constructor() { }

  ngOnInit(): void {
  }

}
