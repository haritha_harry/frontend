import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/Services/auth/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  signinForm : FormGroup

  submitted = false

  user:any


  constructor(private formbuilder:FormBuilder,
              private router:Router,
              private toastr: ToastrService,
              private auth:AuthService) {

    if (auth.checkIfAuthenticated()){
      this.router.navigateByUrl('/home');
    }
   }

  ngOnInit(): void {
    this.signinForm = this.formbuilder.group({
      email : ['',[Validators.required,Validators.email]],
      password :['',[Validators.required,Validators.minLength(8)]]
    })
  }

  get h(){
    return this.signinForm.controls;
  }
  
  async onSubmit(){
    this.submitted = true

    if(this.signinForm.invalid){
      console.log("INVALID SIGNIN DETAILS");    
      return;  
    }
    const {email,password} = this.signinForm.value;
    const username = email
    this.user = {username,password}
   
    this.auth.signin(this.user).then((data)=>{
      console.log(data);
      localStorage.setItem('signin', data.attributes.email);
      this.toastr.success("Sign in success")
      this.router.navigateByUrl("/home")
    }).catch((err) => {
      console.log(err)
      this.toastr.error(err.message)
    });       
    this.user = null    
  }
}
