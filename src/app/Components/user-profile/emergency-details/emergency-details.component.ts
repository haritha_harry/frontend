import { SEMERGENCY, PEMERGENCY } from '../../../constants/user-details';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-emergency-details',
  templateUrl: './emergency-details.component.html',
  styleUrls: ['./emergency-details.component.scss']
})
export class EmergencyDetailsComponent implements OnInit {
  primary = PEMERGENCY;
  secondary = SEMERGENCY;
  constructor() { }

  ngOnInit(): void {
  }

}
