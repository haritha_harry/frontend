import { PERSONAL } from '../../../constants/user-details';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {

  dataSource = PERSONAL
  constructor() { }

  ngOnInit(): void {
  }

}
