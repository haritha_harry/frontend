import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from 'aws-amplify';
import { ToastrService } from 'ngx-toastr';




@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {

  code:any
  username : string

  constructor(private router:Router,private toastr : ToastrService) { 
    this.username = localStorage.getItem('signin')
  }

  ngOnInit(): void {
  } 


  confirmSignUp() {   
      Auth.confirmSignUp(this.username, this.code).then((data)=>{
        console.log(data)
          this.toastr.success('Verification Success')
        this.router.navigateByUrl("/signin")
      }).catch((err)=>{
        this.toastr.error('Verification Failed')
        this.router.navigateByUrl('/signup')
      })    
  }
}
