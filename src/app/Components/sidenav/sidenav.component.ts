import { SidenavService } from '../../services/sidenav.service';
import { Component, OnInit } from '@angular/core';
import {
  DASHBOARD,
  APPS,
  EMPLOYEES,
  PROJECTS,
  ACCOUNTS,
  PAYROLL,
  REPORTS,
  PERFORMMANCE,
  TRAINING,
  AUTHENTICATION,
} from '../../constants/data';
import { onSideNavChange } from 'src/app/animations/animation';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  animations:[onSideNavChange]
})
export class SidenavComponent implements OnInit {
  
  showdashboard = false;
  showapps = false;
  showemployees = false;
  showprojects = false;
  showaccounts = false;
  showpayroll = false;
  showreports = false;
  showperform = false;
  showtraining = false;
  showauth = false;
  dashboard = DASHBOARD;
  apps = APPS;
  employees = EMPLOYEES;
  projects = PROJECTS;
  accounts = ACCOUNTS;
  payroll = PAYROLL;
  reports = REPORTS;
  training = TRAINING;
  performmance = PERFORMMANCE;
  authentication = AUTHENTICATION;
  sideNavState: boolean = false;
  linkText: boolean = false;

  constructor(private _sidenavService: SidenavService) { }

  ngOnInit(): void {
  }
  
  toggleList(listName: string) {
    console.log(listName);
    if (listName === 'dashboard') {
      this.showdashboard = !this.showdashboard;
    } else if (listName === 'apps') {
      this.showapps = !this.showapps;
    } else if (listName === 'employees') {
      this.showemployees = !this.showemployees;
    } else if (listName === 'projects') {
      this.showprojects = !this.showprojects;
    } else if (listName === 'accounts') {
      this.showaccounts = !this.showaccounts;
    } else if (listName === 'payroll') {
      this.showpayroll = !this.showpayroll;
    } else if (listName === 'reports') {
      this.showreports = !this.showreports;
    } else if (listName === 'performmance') {
      this.showperform = !this.showperform;
    } else if (listName === 'training') {
      this.showtraining = !this.showtraining;
    } else if (listName === 'authentication') {
      this.showauth = !this.showauth;
    }
  }
  onSidenavToggle() {
    this.sideNavState = !this.sideNavState
    
    setTimeout(() => {
      this.linkText = this.sideNavState;
    }, 200)
    this._sidenavService.sideNavState$.next(this.sideNavState)
  }

  onClick(name){
    console.log(name)
    return name;
  }
}
