import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from 'aws-amplify';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  username = null

  constructor(private router:Router,private toastr:ToastrService) {    
      this.checkIfAuthenticated();  
   }

  ngOnInit(): void {
  }  

  checkIfAuthenticated(){
    Auth.currentAuthenticatedUser().then((user) => {
      // console.log(user+"FORM HOME");   
      this.username = user.attributes.email;   
  }, (error) => {
    console.log(error)
    this.toastr.info("Sign in to view home page")
    this.router.navigateByUrl("/signin")
  });
  }

}
