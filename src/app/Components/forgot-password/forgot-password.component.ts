import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from 'aws-amplify';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  username :string =""
  code:any
  new_password :string
  submitted = false

  constructor(private toastr:ToastrService,private router : Router) {
    this.username = localStorage.getItem('signin')
   }

  ngOnInit(): void {
  }

  forgotPassword(){
    if(this.username!=null){
      this.submitted = true
    }
    Auth.forgotPassword(this.username)
    .then(data => console.log(data))
    .catch(err => console.log(err));
    }

    verifyAndChangePassword(){
      Auth.forgotPasswordSubmit(this.username, this.code, this.new_password)
    .then((data)=>{
      this.toastr.success("Password Changed Successfully")
      this.toastr.info("Please Sign in with new password")
      
      this.router.navigateByUrl("/signin")
    })
    .catch((err)=>{
      this.toastr.error(err.message)
      this.router.navigateByUrl("/signin")
    });
      this.username =""
      this.new_password=""
      this.code=""
      this.submitted=false
    }
    
}



// Send confirmation code to user's email
