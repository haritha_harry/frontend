import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import { ResizableModule } from 'angular-resizable-element';

import { ReportsRoutingModule } from './reports-routing.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ReportsComponent } from './reports.component';
import { ResultsComponent } from './results/results.component';
import { CourseProgressComponent } from './course-progress/course-progress.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [WelcomeComponent, ReportsComponent, ResultsComponent, CourseProgressComponent, ProfileComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    MatCardModule,
    MatProgressBarModule,
    MatTableModule,
    ResizableModule,
    NgCircleProgressModule.forRoot({
      "subtitle" : "Progress",
      "subtitleFontSize" : "15"
    })
  ],
  exports: [
    WelcomeComponent,
    ReportsComponent
  ]
})
export class ReportsModule { }
