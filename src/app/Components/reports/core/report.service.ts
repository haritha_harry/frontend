import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor() { }
  message = [
    {"id" : 1, "technology" : "Angular", "value" : "50" },
    {"id" : 2, "technology" : "React", "value" : "60" },
    {"id" : 3, "technology" : "Vue", "value" : "40" },
    {"id" : 4, "technology" : "Node", "value" : "80" }

  ]
  courseProgress = [
    {"id" : 1,"img" : "https://angular.io/assets/images/logos/angular/angular.svg", "course" : "Angular Beyond The Basics", "progress" : "30"},
    {"id" : 2,"img" : "https://cdn.auth0.com/blog/react-js/react.png", "course" : "React Development Course", "progress" : "50"},
    {"id" : 3,"img" : "https://bit.ly/3aWIPuQ", "course" : "AWS from Scratch", "progress" : "80"},
    {"id" : 4,"img" : "https://vuejs.org/images/logo.png", "course" : "Vue Basics", "progress" : "100"},
    {"id" : 5,"img" : "https://webassets.mongodb.com/_com_assets/cms/MongoDB_Logo_FullColorBlack_RGB-4td3yuxzjs.png", "course" : "MongoDB Basics", "progress" : "20"}
  ];
  getMessages() {
    return this.message;
  }
  getCourseProgress() {
    return this.courseProgress;
  }
}
