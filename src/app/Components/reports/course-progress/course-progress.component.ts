import { Component, Input, OnInit } from '@angular/core';
import { Chart } from 'node_modules/chart.js'; 
import { ReportService } from '../core/report.service';

@Component({
  selector: 'app-course-progress',
  templateUrl: './course-progress.component.html',
  styleUrls: ['./course-progress.component.scss']
})
export class CourseProgressComponent implements OnInit {

  dataSource : Object = {};
  LineChart=[];
  displayedColumns: string[] = ['id', 'img', 'course', 'progress'];
  constructor(private reportService : ReportService) { }

  ngOnInit(): void {
    this.dataSource = this.reportService.getCourseProgress();
    console.log(this.dataSource);
    this.LineChart = new Chart('lineChart', {
      type : 'line',
      data : {
        labels : ["Jan", "Feb", "Mar", "April", "May","June","July","Aug","Sept"],
        datasets : [{
          label : 'Courses Completed',
          data : [2,4,5,7,9,11,12,12,13],
          fill : true,
          lineTension : 0.1,
          borderColor : "#63c2ff",
          borderWidth : 4,
          
        }]
      },
      options :  {
        responsive : false,
        title : {
          text : "Line Chart",
          display : true
      },
      scales : {
        yAxes : [{
          ticks : {
            beginAtZero : true
          }
        }]
      }
    }
    });
  }
  
}
