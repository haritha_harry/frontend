import { Component, Input, OnInit } from '@angular/core';
import { ReportService } from '../core/report.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  message;
  constructor(private reportservice : ReportService) { 
  }

  ngOnInit(): void {
    this.message = this.reportservice.getMessages();
  }
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

}
