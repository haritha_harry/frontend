import { DragAndDropComponent } from './components/drag-and-drop/drag-and-drop.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AdminComponent } from './components/admin/admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { HomeComponent } from './Components/home/home.component';
import { SigninComponent } from './Components/signin/signin.component';
import { SignupComponent } from './Components/signup/signup.component';
import { VerifyEmailComponent } from './Components/verify-email/verify-email.component';
import { ForgotPasswordComponent } from './Components/forgot-password/forgot-password.component';

const routes: Routes = [
  { path: '', component: AdminComponent },
  { path: 'Admin-Dashboard', component: AdminComponent },
  { path: 'Employee-Dashboard', component: ReportsComponent },
  {path : 'profile', component: UserProfileComponent},
  {path: 'Tasks', component: DragAndDropComponent},
  {path:'signup',component:SignupComponent  },
  { path:'signin',component:SigninComponent  },
  { path:'home',component:HomeComponent  },
  { path : 'verification',component : VerifyEmailComponent  },
  { path: 'forgotPassword',component : ForgotPasswordComponent  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
