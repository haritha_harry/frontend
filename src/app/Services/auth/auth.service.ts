import { Injectable } from '@angular/core';
import { Auth } from 'aws-amplify';
import {from, Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  islogin = false

  constructor() { }

  checkIfAuthenticated(){
    
    Auth.currentAuthenticatedUser().then((user)=>{
      this.islogin = true
    }).catch(err =>{
      this.islogin = false
    })
    return this.islogin
  }

  async signin(user){
    const data = await Auth.signIn(user)
    return data
 }

 async signup(user){
  const data = await Auth.signUp(user)
  return data 
 }

 async signout(){
  const data =   await Auth.signOut()
  return data 
  }
}
