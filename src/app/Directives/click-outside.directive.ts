import { Directive, ElementRef, EventEmitter, HostListener, OnInit,Output,Renderer2 } from '@angular/core';
@Directive({
  selector : '[appClickOutside]'
})
export class ClickOutsideDirective implements OnInit{
  constructor(private elementRef: ElementRef){
    console.log("constructor called")
  }
  ngOnInit(){}
  @Output() appClickOutside = new EventEmitter();
    
  @HostListener('document:click', ['$event.target']) onClick(targetElement){
    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if(!clickedInside) {
      console.log("cli",!clickedInside)
        this.appClickOutside.emit(null);
    }
  }

}