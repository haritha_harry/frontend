export const DASHBOARD = [
  { id: 1, listName: 'Admin-Dashboard' },
  { id: 2, listName: 'Employee-Dashboard' },
];

export const APPS = [
  { id: 1, listName: 'Chat' },
  { id: 2, listName: 'Calls' },
  { id: 3, listName: 'Calendar' },
  { id: 4, listName: 'Contacts' },
  { id: 5, listName: 'Email' },
  { id: 6, listName: 'File Manager'}

  
];
export const EMPLOYEES = [
  { id: 1, listName: 'All Employees' },
  { id: 2, listName: 'Holidays' },
  { id: 3, listName: 'Attendance' },
  { id: 4, listName: 'Timesheet' },
  { id: 5, listName: 'Overtime' },
 
  
];
export const PROJECTS = [
  { id: 1, listName: 'Projects' },
  { id: 2, listName: 'Tasks' },
  { id: 3, listName: 'Task Board' }, 
  
];

export const ACCOUNTS = [
  { id: 1, listName: 'Estimates' },
  { id: 2, listName: 'Invoice' },
  { id: 3, listName: 'Payment' },
  { id: 4, listName: 'Expenses' },
  { id: 5, listName: 'Provident Fund' },
  { id: 6, listName: 'Taxes'}

  
];
export const PAYROLL = [
  { id: 1, listName: 'Employee Salary' },
  { id: 2, listName: 'Payslip' },
  { id: 3, listName: 'Payroll Items' }, 
  
];

export const REPORTS = [
  { id: 1, listName: 'Expense Reports' },
  { id: 2, listName: 'Invoice Reports' },
];

export const PERFORMMANCE = [
  { id: 1, listName: 'Performmance Indicator' },
  { id: 2, listName: 'Performmance Review' },
  { id: 3, listName: 'Performmance Appraisal' }, 
  
];
export const TRAINING = [
  { id: 1, listName: 'Training List' },
  { id: 2, listName: 'Training Type' },
  { id: 3, listName: 'Trainers' }, 
  
];
export const AUTHENTICATION = [
  { id: 1, listName: 'Login' },
  { id: 2, listName: 'Register' },
  { id: 3, listName: 'Forgot Passowrd' }, 
  {id: 4, listName : 'OTP'}
  
];

