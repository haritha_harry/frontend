export const TASKS=[
    {
      id:1,
      name:"add authentication",
      description:["enter user details and authenticate"],
      status:'incomplete',
      time:'11:44'
    },
    {
      id:2,
      name:"add navbar",
      description:["add navigation bar at the top "],
      status:'incomplete',
      time:'01:44'
    },
    {
      id:3,
      name:"bug fix",
      description:["user details are not storing in the database"],
      status:'incomplete',
      time:'12:34'
    },
    {
      id:4,
      name:"enable file upload",
      description:["add a feature for the file upload for storing user image and the image size should be less than 6Mb"],
      status:'inProgress',
      time:'03:12',
      email:'user email',
    },
    {
      id:5,
      name:"Email feature",
      description:["add a email feature to rest the password "],
      status:'completed',
      time:'06:01'
    },
    {
      id:6,
      name:"create profile dashboard",
      description:["a component to see all the details of the user"],
      status:'completed',
      time:'08:00'
    },
  ]

  export const INCOMPLETE="incomplete";
  export const INPROGRESS="inProgress";
  export const COMPLETED="completed";