export const PROFILE = [
  {
    id: 1,
    phone: '9090909090',
    email: 'haritha.kj@presidio.com',
    dob: '5th June 2000',
    address: '1861 Bayonne Ave, Manchester Township, NJ, 08759',
    gender: 'Female',
  },
];

export const PERSONAL =[
  {
    id: 1,
    passportno: '9090909090',
    tel : '9876543210',
    nationality: 'INDIAN',
    religion: 'HINDU',
    mstatus: 'Unmarried',
    emp: 'No',
    childcount: '0',
  },
]

export const PEMERGENCY = [
  {
    id: 1,
    name: 'John',
    relationship: 'Uncle',
    phone: '9292929292',
  },
]
export const SEMERGENCY =[
  {
    id: 1,
    name: 'San',
    relationship: 'Father',
    phone: '9393939393',
  },
]
