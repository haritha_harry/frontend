import { PersonalDetailsComponent } from './components/user-profile/personal-details/personal-details.component';
import { ExperienceDetailsComponent } from './components/user-profile/experience-details/experience-details.component';
import { EduDetailsComponent } from './components/user-profile/edu-details/edu-details.component';
import { EmergencyDetailsComponent } from './components/user-profile/emergency-details/emergency-details.component';
import { ProfileDetailsComponent } from './components/user-profile/profile-details/profile-details.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { HeaderComponent } from './components/header/header.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { CardComponent } from './components/card/card.component';
import { StepperFormComponent } from './components/stepper-form/stepper-form.component';
import { ReportsModule } from './components/reports/reports.module';
import { AdminModule } from './components/admin/admin.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { TaskBoxComponent } from './components/task-box/task-box.component';
import { DragAndDropComponent } from './components/drag-and-drop/drag-and-drop.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import { FlexLayoutModule } from '@angular/flex-layout';
import {DragAndDropService} from './services/dragAndDrop/drag-and-drop.service';
import { VerifyEmailComponent } from './Components/verify-email/verify-email.component';
import { ForgotPasswordComponent } from './Components/forgot-password/forgot-password.component'
import {SignupComponent} from "./Components/signup/signup.component"
import {SigninComponent } from "./Components/signin/signin.component"
import { HomeComponent} from "./Components/home/home.component"
import {NavbarComponent} from "./Components/navbar/navbar.component"

import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [
    AppComponent,
    TaskBoxComponent,
    DragAndDropComponent,
    StepperFormComponent,
    CardComponent,
    DialogComponent,
    HeaderComponent,
    SidenavComponent,
    UserProfileComponent,
    ProfileDetailsComponent,
    PersonalDetailsComponent,
    EmergencyDetailsComponent,
    EduDetailsComponent,
    ExperienceDetailsComponent,
    VerifyEmailComponent,
    ForgotPasswordComponent,
    SignupComponent,
    SigninComponent,
    HomeComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    AdminModule,
    ReportsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    MatDividerModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatExpansionModule,
    FlexLayoutModule,
    ToastrModule.forRoot(), // ToastrModule added  
    
  ],
  providers: [
    DragAndDropService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
